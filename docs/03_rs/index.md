---
author: Sophie CHANE-LUNE
title: Réseaux sociaux
---
# LES RÉSEAUX SOCIAUX

!!! tip "Ce qu'il faut savoir"

    - Connaître les principaux concepts liés à l’usage des réseaux sociaux.
    - Distinguer plusieurs réseaux sociaux selon leurs caractéristiques, y compris un ordre de grandeur de leurs nombres d’abonnés.
    - Paramétrer des abonnements pour assurer la confidentialité de données personnelles.
    - Identifier les sources de revenus des entreprises de réseautage social.
    - Déterminer ces caractéristiques sur des graphes simples.
    - Décrire comment l’information présentée par les réseaux sociaux est conditionnée par le choix préalable de ses amis.
    - Connaître les dispositions de l’article 222-33-2-2 du code pénal.
    - Connaître les différentes formes de cyberviolence (harcèlement, discrimination, sexting...) et les ressources disponibles pour lutter contre la cyberviolence.

!!! tip "Ce qu'il faut savoir faire"
    - Construire ou utiliser une représentation du graphe des relations d’un utilisateur.
    S’appuyer sur la densité des liens pour identifier des groupes, des communautés.
    - Sur des exemples de graphes simples, en informatique débranchée, étudier les notions de rayon, diamètre et centre d’un graphe, de manière à illustrer la notion de « petit monde ».
    - Comparer les interfaces et fonctionnalités de différents réseaux sociaux.
    - Dresser un comparatif des formats de données, des possibilités d’échange ou d’approbation (bouton like), de la persistance des données entre différents réseaux sociaux.
    - Analyser les paramètres d’utilisation d’un réseau social. Analyser les autorisations données aux applications tierces.
    - Discuter des garanties d’authenticité des comptes utilisateurs ou des images.
    - Lire et expliquer les conditions générales d’utilisation d’un réseau social.
    - Consulter le site [nonauharcelement.education.gouv.fr](http://nonauharcelement.education.gouv.fr).
