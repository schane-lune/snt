---
author: Sophie CHANE-LUNE
title: Photographie numérique
---
# LA PHOTOGRAPHIE NUMÉRIQUE

!!! tip "Ce qu'il faut savoir"

    - Distinguer les photosites du capteur et les pixels de l’image en comparant les résolutions du capteur et de l’image selon les réglages de l’appareil.
    - Retrouver les métadonnées d’une photographie.
    - Traiter par programme une image pour la transformer en agissant sur les trois composantes de ses pixels.
    - Expliciter des algorithmes associés à la prise de vue.
    Identifier les étapes de la construction de l’image finale.

!!! tip "Ce qu'il faut savoir faire"
    - Programmer un algorithme de passage d’une image couleur à une image en niveaux de gris : par moyenne des pixels RVB ou par changement de modèle de représentation (du RVB au TSL, mise de la saturation à zéro, retour au RVB).
    - Programmer un algorithme de passage au négatif d’une image.
    - Programmer un algorithme d’extraction de contours par comparaison entre pixels voisins et utilisation d’un seuil.
    - Utiliser un logiciel de retouche afin de modifier les courbes de luminosité, de contraste, de couleur d’une photographie.