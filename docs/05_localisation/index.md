---
author: Sophie CHANE-LUNE
title: Localisation
---
# LOCALISATION, CARTOGRAPHIE ET MOBILITÉ

!!! tip "Ce qu'il faut savoir"

    - Décrire le principe de fonctionnement de la géolocalisation.
    - Identifier les différentes couches d’information de GeoPortail pour extraire différents types de données.
    - Contribuer à OpenStreetMap de façon collaborative.
    - Décoder une trame NMEA pour trouver des coordonnées géographiques.
    - Utiliser un logiciel pour calculer un itinéraire.
    - Représenter un calcul d’itinéraire comme un problème sur un graphe.
    - Régler les paramètres de confidentialité d’un téléphone pour partager ou non sa position.


!!! tip "Ce qu'il faut savoir faire"
    - Expérimenter la sélection d’informations à afficher et l’impact sur le changement d’échelle de cartes (par exemple sur GeoPortail), ainsi que les ajouts d’informations par les utilisateurs dans OpenStreetMap.
    - Mettre en évidence les problèmes liés à un changement d’échelle dans la représentation par exemple des routes ou de leur nom sur une carte numérique pour illustrer l’aspect discret du zoom.
    - Calculer un itinéraire routier entre deux points à partir d’une carte numérique.
    - Connecter un récepteur GPS sur un ordinateur afin de récupérer la trame NMEA, en extraire la localisation.
    - Extraire la géolocalisation des métadonnées d’une photo.
    - Situer sur une carte numérique la position récupérée.
    - Consulter et gérer son historique de géolocalisation.