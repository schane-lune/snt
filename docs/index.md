# SNT

## Programme EDUSCOL
[Programme de SNT](https://eduscol.education.fr/document/23494/download){ .md-button target="_blank" rel="noopener"}

## Déroulement

1. [Internet](./01_internet/index.md)
2. [Le WWW](./02_web/index.md)
3. [Les réseaux sociaux](./03_rs/index.md)
4. [Les données structurées et leur traitement](./04_donnees-structurees/index.md)
5. [Localisation, cartographie et mobilité](./05_localisation/index.md)
6. [Informatique embarquée et objets connectés](./06_objets-connectes/index.md)
7. [La photographie numérique](./07_photographie/index.md)


## Aide-mémoire Python
[PCSHEET !!!](./08_programmation/PyCS/index.md){ .md-button}

lien vidéo :
https://youtu.be/efHWqQirbtc
