---
author: Sophie CHANE-LUNE
title: Informatique embarquée
---
# INFORMATIQUE EMBARQUÉE ET OBJETS CONNECTÉS

!!! tip "Ce qu'il faut savoir"

    - Identifier des algorithmes de contrôle des comportements physiques à travers les données des capteurs, l’IHM et les actions des actionneurs dans des systèmes courants.
    - Réaliser une IHM simple d’un objet connecté.
    - Écrire des programmes simples d’acquisition de données ou de commande d’un actionneur.


!!! tip "Ce qu'il faut savoir faire"
    - Identifier les évolutions apportées par les algorithmes au contrôle des freins et du moteur d’une automobile, ou à l’assistance au pédalage d’un vélo électrique.
    - Réaliser une IHM pouvant piloter deux ou trois actionneurs et acquérir les données d’un ou deux capteurs.
    - Gérer des entrées/sorties à travers les ports utilisés par le système.
    - Utiliser un tableau de correspondance entre caractères envoyés ou reçus et commandes physiques (exemple : le moteur A est piloté à 50 % de sa vitesse maximale lorsque le robot reçoit la chaîne de caractères « A50 »).
