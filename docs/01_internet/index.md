---
author: Sophie CHANE-LUNE
title: Internet
---
# INTERNET

!!! tip "Ce qu'il faut savoir"

    - Distinguer le rôle des protocoles IP et TCP.
    - Caractériser les principes du routage et ses limites.
    - Distinguer la fiabilité de transmission et l’absence de garantie temporelle.
    - Sur des exemples réels, retrouver une adresse IP à partir d’une adresse symbolique et inversement.
    - Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages illicites qu’on peut en faire.
    - Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non.
    - Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution.

!!! tip "Ce qu'il faut savoir faire"
    - Illustrer le fonctionnement du routage et de TCP par des activités débranchées ou à l’aide de logiciels dédiés, en tenant compte de la destruction de paquets.
    - Déterminer l’adresse IP d’un équipement et l’adresse du DNS sur un réseau.
    - Analyser son réseau local pour observer ce qui y est connecté.
    - Suivre le chemin d’un courriel en utilisant une commande du protocole IP.
