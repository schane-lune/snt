---
author: Sophie CHANE-LUNE
title: Données structurées
---
# LES DONNÉES STRUCTURÉES ET LEUR TRAITEMENT

!!! tip "Ce qu'il faut savoir"

    - Définir une donnée personnelle.
    - Identifier les principaux formats et représentations de données.
    - Identifier les différents descripteurs d’un objet.
    - Distinguer la valeur d’une donnée de son descripteur.
    - Utiliser un site de données ouvertes, pour sélectionner et récupérer des données.
    - Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables.
    - Retrouver les métadonnées d’un fichier personnel.
    - Utiliser un support de stockage dans le nuage.
    - Partager des fichiers, paramétrer des modes de synchronisation.
    - Identifier les principales causes de la consommation énergétique des centres de données ainsi que leur ordre de grandeur.

!!! tip "Ce qu'il faut savoir faire"
    - Consulter les métadonnées de fichiers correspondant à des informations différentes et repérer celles collectées par un dispositif et celles renseignées par l’utilisateur.
    - Télécharger des données ouvertes (sous forme d’un fichier au format CSV avec les métadonnées associées), observer les différences de traitements possibles selon le logiciel choisi pour lire le fichier : programme Python, tableur, éditeur de textes ou encore outils spécialisés en ligne.
    - Explorer les données d’un fichier CSV à l’aide d’opérations de tri et de filtre, effectuer des calculs sur ces données, réaliser une visualisation graphique des données.
    - À partir de deux tables de données ayant en commun un descripteur, montrer l’intérêt des deux tables pour éviter les redondances et les anomalies d’insertion et de suppression, réaliser un croisement des données permettant d’obtenir une nouvelle information.
    - Illustrer, par des exemples simples, la consommation énergétique induite par le traitement et le stockage des données.