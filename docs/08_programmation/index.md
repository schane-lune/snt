---
author: Sophie CHANE-LUNE
title: Objectifs
---

# NOTION D'ALGORITHMIQUE ET DE PROGRAMMATION

!!! tip "Ce qu'il faut savoir"

    - Écrire et développer des programmes pour répondre à des problèmes.
    - Écrire et développer des programmes pour modéliser des phénomènes physiques, économiques et sociaux.

!!! tip "Ce qu'il faut savoir faire"
    - Illustrer ces notions par des activités liées aux différents thèmes du programme.