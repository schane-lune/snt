---
author: Sophie CHANE-LUNE
title: Programmation
---

# NOTION DE PROGRAMMATION
Quand on a bien réfléchi à l'algorithme, il ne reste plus qu'à le traduire dans un langage compréhensible par une machine. Un **langage de programmation** définit un ensemble d'éléments et de règles syntaxiques pour écrire le code source d'un programme. Le langage de programmation utilisé est Python 🐍.  

!!! info "Définition : *Écrire un programme*"
    Un **programme** est une traduction d'un algorithme dans un langage exécutable par une machine.  

!!! info "Définition : *Éxécuter un programme*"
    **Exécuter** un programme consiste à demander à une machine de réaliser la suite d'instructions qui le compose.  
    Une **console de programmation Python 🐍** permet d'exécuter des instructions ou un programme écrits en Python.

???+ question "IDE Python 🐍"

    Un IDE, ou environnement de développement intégré, est un environnement de travail qui regroupe plusieurs outils essentiels pour concevoir, écrire, exécuter, tester et déboguer des programmes informatiques. L'espace de travail ci-dessous se divise en deux parties principales : à gauche, un éditeur de texte dédié à la rédaction du code, et à droite, une console ou un espace d'exécution où les programmes peuvent être testés et débogués.  

    {{IDEv()}}


!!! warning "Remarque"
    La différence entre un algorithme et un programme se résume à leur public cible : l'un est destiné à être compris par un être humain, tandis que l'autre est conçu pour être exécuté par une machine.

## 1. Traduction des instructions et des expressions
### Les instructions
Les instructions d'un algorithme se traduisent en Python en respectant la syntaxe du langage.  

!!! note "Traduction de l'affectation"
    Affecter la valeur `a` à une variable `v`, notée `v ← a`, s'écrit sous Python 🐍 : `v = a`.

!!! note "Traduction des instructions de lecture et d'écriture"
    Écrire un message et la valeur d'une d'une variable `v`, s'écrit sous Python 🐍 : `print("message", v)`.  
    Lire une chaîne, l'affecter dans la variable `c`, s'écrit sous Python : `c = input(message)`.

### Les expressions
!!! info "Définition : *Expression*"
    Une **expression** est une combinaison de constantes, de valeurs des variables, d'opérateurs et des résultats de fonctions qui est évaluée pour produire une valeur. Les expressions peuvent être simples, comme une valeur littérale (par exemple, un nombre entier, un nombre à virgule ou une chaîne de caractères), ou complexes, impliquant des opérations.

??? note "Expression arithmétique"
    Une expression arithmétique est une combinaison de nombres et d'opérations arithmétiques et s'écrit sous 🐍 :

    - valeurs :  

        - Nombres entiers (de type `int`) : `5`, `-20`, `987654321`  
        - Nombres à virgules flottantes (de type `float`) : `1.5`, `3.14159`, `-6.5`, `.5`, `-1.`, `5e3`, `-123.4e-5`  

    - opérations arithmétiques :

        - addition : `+`  
        - soustraction : `-`  
        - multiplication : `*`  
        - division : `/`  
        - puissance : `**`  
        - division entière : `//`  
        - reste de la division entière : `%`  

    Ordre de priorité : `[]`, `()`, `**`, `*`, `/`, `//`, `%`, `+`, `-`

??? note "Expression chaîne de caractères"
    Une chaîne de caractères est une combinaison de caractères et d'opérations et s'écrit sous 🐍 :

    - valeurs :  "NSI", 'NSI' (de type `string`)  

    - opérations :  

        - concaténation : `+`
        - répétition : `*`

!!! info "Définition : *Condition*"
    Une **condition** (ou **expression booléenne**) est une expression dont l'évaluation donnera soit `vraie` ou `fausse`.  

??? note "Expression booléenne"
    Une expression booléenne est une combinaison de valeur de vérité et d'opérations et s'écrit sous 🐍 :

    - valeurs :  `True`, `False` (de type `bool`)
    - opérateurs logiques : `or`, `and`, `not`, `in`, `not in`  
    - opérateurs de comparaisons : `==`, `!=`, `<`, `<=`, `>`, `>=`

!!! warning "Remarque"
    En Python 🐍, l'affectation est notée avec un signe `=` alors que l'opérateur d'égalité est notée `==`.

## 2. Traduction des instructions composées
Pour traduire un algorithme en un programme Python 🐍, chaque construction doit être traduite en la construction Python correspondante, en tenant compte de l'**indentation** pour marquer et délimiter les blocs imbriqués, tout en respectant la syntaxe du langage Python. L'indentation, qui est le décalage des instructions par rapport à la marge, est essentielle. Si plusieurs instructions se suivent, elles doivent être alignées à gauche avec la même indentation.  

??? note "SI ALORS"
    La conditionnelle est notée :  
    ```Algo
    SI condition
        instructions
    ```

    Elle teste si la `condition` est vraie :  

    - Si la `condition` est vraie (`True`), le bloc d'instructions indenté après le test est exécuté.  
    - Si la `condition` est fausse , elles ne sont pas exécutées et on passe directement à la suite.


    ```python
    if condition :                  # Ne pas oublier le signe de ponctuation :
        bloc_instructions         # Attention à l'indentation
    ``` 

??? note "SI ALORS SINON"
    L'alternative est notée :  
    ```Algo
    SI condition
        instructions 1
    SINON
        instructions 2
    ```

    Elle teste si la `condition` est vraie :

    - Si la `condition` est vraie  (`True`), le bloc d'instructions indenté après le test est exécuté.  
    - Si la `condition` est fausse (`False`), le bloc d'instructions indenté après le `else` est exécuté.
    
    ```python
    if condition :
        bloc_instructions_1
    else :
        bloc_instructions_2
    ``` 

??? note "SI ALORS SINON SI"
    On peut imbriquer plusieurs options de la manière suivante :
    ```Algo
    SI condition
        instructions 1
    SINON SI
        instructions 2
    SINON SI
        instructions 3
    ...
    ALORS
        instructions
    ```

    L’instruction `elif` est équivalente à `else if`.
    ```python
    if condition :
        bloc_instructions_1
    elif :
        bloc_instructions_2
    elif :
        bloc_instructions_3
    ...
    else :
        bloc_instructions
    ``` 

??? note "REPETER ... FOIS"
    ```Algo
    REPETER ... FOIS
        instructions
    ```

    Pour exécuter `n` fois le bloc d'instructions indenté, à l’aide d’une variable entière `variable` allant de `0` à `n-1` :  
    ```python
    for variable in range(n) :
        bloc_instructions
    ```

    Si la variable varie d'un entier `m` à `n-1` (`n>m`), on remplace la première ligne par :  
    ```python
    for variable in range(m,n) :
        bloc_instructions
    ```

    Si la variable varie d'un entier `m` à `n-1` (`n>m`) par pas de `p`, on remplace la première ligne par :  
    ```python
    for variable in range(m,n,p) :
        bloc_instructions
    ```

??? note "TANT QUE"
    ```Algo
    TANT QUE condition
        instructions
    ```

    Pour exécuter en boucle le bloc d'instructions indenté tant que la condition est vraie (`True`) :  
    ```python
    while condition :
        bloc_instructions
    ```

## 3. Structuration des programmes
Quand cela devient trop compliqué, on peut décomposer un problème complexe en plusieurs problèmes plus simples. La programmation structurée consiste à décomposer un programme en sous-programme, en encapsulation des blocs de code réutilisables dans des **fonctions**.


!!! info "Définition : *Fonction*"
    Sous Python 🐍, une **fonction** permet d'écrire une partie de programme en lui donnant un nom et éventuellement, un ou plusieurs paramètres. Pour l'utiliser il suffit simplement de donner son nom et les valeurs particulières à utiliser pour les paramètres.  

    1. Définition d'une fonction  

        La définition d'une fonction s'écrit en deux parties:  

        - l'**entête** (ou la signature de la fonction), définit le nom de la fonction, ses paramètres d'entrée (nommés aussi paramètres formels) et son type de retour
        - le **corps** de la fonction est le bloc d'instructions à exécuter quand on appelle la fonction.  

        ```python
        def nom_de_la_fonction(param1, param2, ...):
            # Corps de la fonction
            # Instructions à exécuter lorsque la fonction est appelée
            return valeur_de_retour
        ```

    2. Appel d'une fontion  

        Une fois la fonction définie, l'exécution du corps de la fonction nécessite qu'elle soit appelée dans une console ou dans un programme. L'instruction d'appel est le nom de la fonction appelée suivie de parenthèses entre lesquelles doivent figurer les **arguments** (ou paramètres effectifs), c'est-à-dire des valeurs correspondant aux types de données de ses paramètres formels dans le même ordre que celui défini dans l'entête.  
        
        ```python
        resultat = nom_de_la_fonction(argument1, argument2, ...)
        ```


!!! example "Exemple"
    La fonction `motif` définie par le mot-clé `def` permet de dessiner le motif dont la taille est précisée en paramètre. Puis pour déssiner la figure complète, on appelle 4 fois la fonction `motif` ainsi définie.

    ```python hl-lines="2 10"
        from turtle import *
        def motif(taille):  # (1)
            for _ in range(3):
                forward(taille)
                right(90)
            left(90)
            left(90)
    
        for i in range(4):
            motif(10)  # (2)

    ```

    1. :warning: définition de la fonction `motif`

    2. :warning: appel de la fonction `motif`

    


!!! warning "Remarque"
    L'exécution d'une fonction peut retourner une valeur si cela a été prévu dans l'écriture de la fonction, par l'instruction `return`. Cette valeur peut être utilisée dans une autre instruction.
