---
author: Sophie CHANE-LUNE
title: Algorithmique
---

# NOTION D'ALGORITHMIQUE

L'**informatique** est la science du traitement automatisé des informations par des **machines** telles que les ordinateurs, les tablettes, les téléphones intelligents ou même les calculatrices. Une **information**, quant à elle, représente une donnée structurée, enregistrée, transmise ou échangée dans le but d'accomplir une tâche spécifique. En somme, l'informatique représente l'ensemble des connaissances et des outils nécessaires pour comprendre et manipuler efficacement le numérique. 

## 1. Informations
Les types élémentaires des informations sont :

- les textes que l'on nomme **chaînes de caractères** 
- les **nombres entiers** et les **nombres à virgules**
- les **booléens** : informations qui n'a que deux valeurs notées `vraie` ou `fausse`.


### Mémorisation des informations  
!!! info "Définition : *Variable*"
    Une **variable** est un emplacement spécifique de la mémoire de l'ordinateur, identifié par un **nom** unique, où une **valeur** peut être stockée et modifiée pendant l'exécution d'un programme. C'est comme une boîte virtuelle dans laquelle nous pouvons stocker différentes informations, telles que des nombres, des mots ou d'autres données. Le nom d'une variable est utilisé pour faire référence à sa valeur dans le programme. 

!!! info "Définition : *Affectation*"
    L'**affectation** est une instruction, notée `v ← a` qui consiste à attribuer une valeur `a` à une variable `v`. 

### Communication des informations
!!! info "Définition : *Les instructions de lecture et d'écriture*"
    Pour programmer les interactions avec l'utilisateur, on utilise des instructions de lecture et d'écriture.

    - `LIRE` permet de demander une information à l'utilisateur et l'enregistre en mémoire.  
    - `ÉCRIRE` permet d'afficher à l'écran ou sur une sortie, la valeur d'une variable en mémoire.  

## 2. Algorithme
!!! info "Définition : *Algorithme*"
    Un **algorithme** est une série d'étapes précises et ordonnées que l'on suit pour résoudre un problème ou accomplir une tâche. Un algorithme est écrit pour un humain afin de lui fournir les instructions détaillées sur la marche à suivre pour atteindre un résultat donné.  

!!! info "Définition : *Séquence*"
    Une **séquence** d'instructions est une suite d'actions disposées successivement dans l'ordre dans lequel elles doivent être exécutées.

!!! example "Exemple"
    La séquence d'instructions correspondant à un virage à droite peut s'écrire ainsi :  
    ```Algo
    avance
    tourne à droite
    avance
    ```

## 3. Instructions conditionnelles  
!!! info "Définition : *Condition*"
    Les **instructions conditionnelles** sont utilisées pour prévoir des exécutions optionnelles.

!!! example "SI ALORS"
    La conditionnelle est notée :  
    ```Algo
    SI condition
        instructions
    ```

    Elle teste si la `condition` est vraie :

    - Si la `condition` est vraie, le bloc d'instructions indenté après le test est exécuté.  
    - Si la `condition` est fausse, elles ne sont pas exécutées et on passe directement à la suite.

!!! example "SI ALORS SINON"
    L'alternative est notée :  
    ```Algo
    SI condition
        instructions 1
    SINON
        instructions 2
    ```
    
    Elle teste si la `condition` est vraie :

    - Si la `condition` est vraie, le bloc d'instructions indenté après le test est exécuté.  
    - Si la `condition` est fausse, le bloc d'instructions indenté après le `SINON` est exécuté.

!!! example "SI ALORS SINON SI"  
    On peut imbriquer plusieurs options de la manière suivante :
    ```Algo
    SI condition
        instructions 1
    SINON SI
        instructions 2
    SINON SI
        instructions 3
    ...
    ALORS
        instructions
    ```

## 4. Instructions itératives  
!!! info "Définition : *Boucle*"
    Une **instruction itérative** ou **boucle** permet de faire exécuter plusieurs fois, une ou plusieurs instructions, en ne les écrivant qu'une seule fois dans l'algorithme ou le programme.  

    Il y a deux sortes de boucles :

    - celles dont on compte le nombre de répétitions, appelées **boucles bornées**  
    - et celles qui s'exécutent jusqu'à ce qu'une condition devienne **vraie** ou **fausse**, appelées **boucles non bornées** 

### Boucles bornées  
!!! info "Définition : *Boucle bornée*"
    La **boucle bornée** permet d'exécuter un nombre de fois prévu à l'avance les instructions indentées.  

!!! example "REPETER ... FOIS"
    ```Algo
    REPETER ... FOIS
        instructions
    ```

### Boucles non bornées  
!!! info "Définition : *Boucle non bornée*"
    La **boucle non bornée** permet de répéter une instruction aussi souvent que nécessaire sans savoir à priori combien de fois.  

!!! example "TANT QUE"
    ```Algo
    TANT QUE condition
        instructions
    ```
    
    Elle teste si la `condition` est vraie :
      
    - Si la `condition` est vraie, le bloc d'instructions indenté est exécuté autant de fois que la `condition`` est vraie.
    - Si la `condition` est fausse, elle s'arrête et on passe directement à la suite.

!!! warning "Remarque"
    Il faut que la condition puisse devenir fausse pour que l'algorithme s'arrête.


## 5. Algorithme complexe  
Quand cela devient trop compliqué, on peut décomposer un problème complexe en plusieurs problèmes plus simples. En algorithmique, une construction est une structure de haut niveau qui regroupe un ensemble d'instructions pour réaliser une tâche ou un comportement spécifique. On peut ainsi imbriquer toutes les instructions ci-dessus (boucles, alternatives, conditionnelles) les unes dans les autres pour faire des algorithmes complexes.  

!!! example "Exemple"
    L'algorithme suivant permet de faire réaliser une trajectoire complexe à un robot. On peut exécuter cet algorithme pas à pas pour la dessiner à la main.

    ```Algo
    REPETER 4 FOIS
        REPETER 3 FOIS
            avance
            tourne à droite
        tourne à gauche
        tourne à gauche
    ```