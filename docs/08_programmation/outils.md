# Outils

## Initiation à la programmation Python
1. [Pyrates](https://py-rates.fr/){:target="_blank" }

## Initiation à l'algorithmique
1. [Blockly Games](https://blockly.games/?lang=fr){:target="_blank" }
2. [Concours Algorea](https://algorea.org/#/){:target="_blank" }

