---
author: Sophie CHANE-LUNE
title: PCSHEET !!!
---
# AIDE-MÉMOIRE 🐍 PYTHON
`#` introduit un **commentaire** qui se poursuit jusqu'à la fin de la ligne.  

## Les types de base
Une **expression** est une combinaison de constantes, de valeurs des variables, d'opérateurs et des résultats de fonctions qui est évaluée et qui produit une nouvelle valeur.  

??? abstract "Les nombres"

    #### 1. Définition  
    Nombres entiers (`int` pour *"integer"* en anglais) : `5`, `-20`, `987654321`  
    Nombres à virgules flottantes (`float` pour *"floating-point number"* en anglais) : `1.5`, `3.14159`, `-6.5`, `.5`, `-1.`, `5e3`, `-123.4e-5`  

    #### 2. Opérateurs arithmétiques  
    - addition : `+`  
    - soustraction : `-`  
    - multiplication : `*`  
    - division : `/`  
    - puissance : `**`  
    - division entière : `//`  
    - reste de la division entière : `%`  

    Ordre de priorité : `[]`, `()`, `**`, `*`, `/`, `//`, `%`, `+`, `-`
        
??? abstract "Les chaînes de caractères"

    #### 1. Définition  
    Chaînes de caractères (`string` pour *"string of characters* en anglais) : `"NSI"`, `'NSI'`  

    #### 2. Opérateurs sur les chaînes de caractères  
    - concaténation : `+`
    - répétition : `*`

??? abstract "Les booléens"

    #### 1. Définition  
    Booléens (`bool` pour *"boolean"* en anglais) : `True`, `False`  

    #### 2. Opérateurs de comparaison  
    - égalité : `==`
    - différence : `!=`
    - tests d'ordre : `<`, `<=`, `>`, `>=`

    #### 3. Opérateurs logiques  
    - disjonction/ou logique : `or`
    - conjonction/et logique : `and`
    - négation : `not`

    #### 4. Opérateurs d'appartenance
    - `in`
    - `not in`

    Priorité croissante des opérateurs dans les expressions :  
    `or`, `and`, `not`, `in`, `not in`, `==`, `!=`, `<`, `<=`, `>`, `>=`, `+`, `-`, `*`, `/`, `//`, `%`, `**`, `()`, `[]`  

## Les structures de contrôle
??? abstract "Les conditionnelles"
    !!! note "SI ALORS"

        <!-- Conditionnelle -->
        - Si la `condition` est vraie (`True`), le bloc d'instructions indenté après le test est exécuté.  
        - Si la `condition` est fausse (`False`), elles ne sont pas exécutées et on passe directement à la suite.

        ```python
        if condition :                  # Ne pas oublier le signe de ponctuation :
            bloc_instructions           # Attention à l'indentation
        ``` 

    !!! note "SI ALORS SINON"

        <!-- Alternative -->
        On peut également préciser des instructions à effectuer si la `condition` est fausse, à l’aide de l’instruction `else` :
        ```python
        if condition :
            bloc_instructions_1
        else :
            bloc_instructions_2
        ``` 

    !!! note "SI ALORS SINON SI"
    
        <!-- Alternative -->
        L’instruction `elif` est équivalente à `else if`.
        ```python
        if condition :
            bloc_instructions_1
        elif :
            bloc_instructions_2
        elif :
            bloc_instructions_3
        ...
        else :
            bloc_instructions
        ``` 

??? abstract "Les boucles"
    ??? abstract "Boucle bornée"

        !!! note "POUR ... ALLANT JUSQU'À ..."

            Pour exécuter `n` fois le bloc d'instructions indenté, à l’aide d’une variable entière `variable` allant de `0` à `n-1` :
            ```python
            for variable in range(n) :
                bloc_instructions
            ```

        !!! note "POUR ... ALLANT DE ... JUSQU'À ..."

            Si la variable varie d’un entier `m` à `n-1` (`n>m`), on remplace la première ligne par :
            ```python
            for variable in range(m,n) :
                bloc_instructions
            ```

        !!! note "POUR ... ALLANT DE .. JUSQU'À ... PAR PAS DE ..."

            Si la variable varie d’un entier `m` à `n-1` (`n>m`) par pas de `p`, on remplace la première ligne par :
            ```python
            for variable in range(m,n,p) :
                bloc_instructions
            ```

    ??? abstract "Boucle non bornée"

        !!! note "TANT QUE"
            Pour exécuter en boucle le bloc d'instructions indenté tant que la condition est vraie (`True`) :
            ```python
            while condition :
                bloc_instructions
            ```
## Les fonctions
??? abstract "Les fonctions"
    
    1. Définition d'une fonction  

        La définition d'une fonction s'écrit en deux parties:  

        - l'**entête** (ou la signature de la fonction), définit le nom de la fonction, ses paramètres d'entrée (nommés aussi paramètres formels) et son type de retour
        - le **corps** de la fonction est le bloc d'instructions à exécuter quand on appelle la fonction.  

        ```python
        def nom_de_la_fonction(param1, param2, ...):
            # Corps de la fonction
            # Instructions à exécuter lorsque la fonction est appelée
            return valeur_de_retour
        ```

    2. Appel d'une fontion  

        Une fois la fonction définie, l'exécution du corps de la fonction nécessite qu'elle soit appelée. L'instruction d'appel est le nom de la fonction appelée suivie de parenthèses entre lesquelles doivent figurer les **arguments** (ou paramètres effectifs), c'est-à-dire des valeurs correspondant aux types de données de ses paramètres formels dans le même ordre que celui défini dans l'entête.  
        
        ```python
        resultat = nom_de_la_fonction(argument1, argument2, ...)
        ```