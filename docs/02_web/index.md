---
author: Sophie CHANE-LUNE
title: Objectifs
---
# LE WEB

!!! tip "Ce qu'il faut savoir"

    - Connaître les étapes du développement du Web  
    - Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien)  
    - Maîtriser les renvois d’un texte à différents contenus  
    - Distinguer ce qui relève du contenu d’une page et de son style de présentation  
    - Etudier et modifier une page HTML simple  
    - Décomposer l’URL d’une page et reconnaître les protocoles
    - Reconnaître les pages sécurisées  
    - Décomposer le contenu d’une requête HTTP et identifier les paramètres passés  
    - Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur  
    - Mener une analyse critique des résultats fournis par un moteur de recherche  
    - Comprendre les enjeux de la publication d’informations  
    - Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d’un navigateur  
    - Sécuriser sa navigation en ligne et analyser les pages et fichiers  
      
!!! tip "Ce qu'il faut savoir faire"

    - Construire une page Web simple contenant des liens hypertextes, la mettre en ligne.  
    - Modifier une page Web existante, changer la mise en forme d’une page en modifiant son CSS. Insérer un lien dans une page Web.  
    - Comparer les paramétrages de différents navigateurs.   
    - Utiliser plusieurs moteurs de recherche, comparer les résultats et s’interroger sur la pertinence des classements.  
    - Réaliser à la main l’indexation de quelques textes sur quelques mots puis choisir les textes correspondant à une requête.  
    - Calculer la popularité d’une page à l’aide d’un graphe simple puis programmer l’algorithme.  
    - Paramétrer un navigateur de manière qu’il interdise l’exécution d’un programme sur le client.  
    - Comparer les politiques des moteurs de recherche quant à la conservation des informations sur les utilisateurs.  
    - Effacer l’historique du navigateur, consulter les cookies, paramétrer le navigateur afin qu’il ne garde pas de traces.  
    - Utiliser un outil de visualisation tel que Cookieviz pour mesurer l’impact des cookies et des traqueurs lors d’une navigation.  
    - Régler les paramètres de confidentialité dans son navigateur ou dans un service en ligne.  

