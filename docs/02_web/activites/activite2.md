---
author: Sophie CHANE-LUNE
title: Activité 2 Fonctionnement d'un moteur de recherche
---
[ACTIVITE 2 Le fonctionnement d'un moteur de recherche](../doc/activite1.pdf){ .md-button target="_blank" rel="noopener" }  
*(Source Delagrave Sciences numériques et Technologie (SNT) 2de (Ed. num. 2022))*